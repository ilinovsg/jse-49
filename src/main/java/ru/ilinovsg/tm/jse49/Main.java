package ru.ilinovsg.tm.jse49;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.ilinovsg.tm.jse49.command.CommandInterface;
import ru.ilinovsg.tm.jse49.formData.FormDataSender;

public class Main {
    public static void main(String[] args) {
        ApplicationContext ctx =
                new AnnotationConfigApplicationContext(AppContext.class);

        CommandInterface commandInterface =(CommandInterface) ctx.getBean("consoleCommandInterfaceImpl");
        FormDataSender formDataSender = (FormDataSender) ctx.getBean("consoleFormDataSender");
        ApplicationProcess applicationProcess = new ApplicationProcess(commandInterface,formDataSender);
        try{
            applicationProcess.progress();
        }catch(RuntimeException e){
            e.getMessage();
        }
    }

}
