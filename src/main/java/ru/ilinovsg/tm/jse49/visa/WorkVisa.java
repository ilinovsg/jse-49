package ru.ilinovsg.tm.jse49.visa;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.ilinovsg.tm.jse49.command.CommandInterface;

@Component
@Scope("prototype")
public class WorkVisa implements VisaApplicationForm{
        private String name;
        private String lastName;

        @Override
        public void fillForm(CommandInterface commandInterface) {
            this.name = commandInterface.writeAndRead("Name: ");
            this.lastName = commandInterface.writeAndRead("LastName: ");
        }

        @Override
        public String getFormData() {
            return "WorkVisa: "+this.name + ", " + this.lastName;
        }

    }
