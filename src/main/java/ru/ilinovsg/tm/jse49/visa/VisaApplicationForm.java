package ru.ilinovsg.tm.jse49.visa;

import ru.ilinovsg.tm.jse49.command.CommandInterface;

public interface VisaApplicationForm {
    void fillForm(CommandInterface commandInterface);

    String getFormData();
}
