package ru.ilinovsg.tm.jse49.formData;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.ilinovsg.tm.jse49.formData.FormDataSender;

@Component
@Scope("prototype")
public class ConsoleFormDataSender implements FormDataSender {
    @Override
    public void send(String data) {
        System.out.println("Call ConsoleFormDataSender");
    }
}
