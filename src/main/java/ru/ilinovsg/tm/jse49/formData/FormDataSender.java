package ru.ilinovsg.tm.jse49.formData;

public interface FormDataSender {
    void send(String data);
}
