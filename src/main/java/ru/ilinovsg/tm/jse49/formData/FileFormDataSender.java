package ru.ilinovsg.tm.jse49.formData;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
public class FileFormDataSender implements FormDataSender {
    @Override
    public void send(String data) {
        System.out.println("Call FileFormDataSender");
    }
}
