package ru.ilinovsg.tm.jse49;

import ru.ilinovsg.tm.jse49.command.CommandInterface;
import ru.ilinovsg.tm.jse49.formData.FormDataSender;
import ru.ilinovsg.tm.jse49.visa.TouristicVisa;
import ru.ilinovsg.tm.jse49.visa.WorkVisa;

public class ApplicationProcess {
    public static final String EXIT = "exit";
    private CommandInterface commandInterface;
    private FormDataSender formDataSender;

    public ApplicationProcess(CommandInterface commandInterface, FormDataSender formDataSender) {
        this.commandInterface = commandInterface;
        this.formDataSender = formDataSender;
    }

    public void progress() throws UnsupportedOperationException {
        String command = "";
        String data = "";
        while (!EXIT.equals(command)) {
            command = commandInterface.writeAndRead("Choose visa type(work, touristic):");
            switch (command) {
                case "work":
                    WorkVisa workVisa = new WorkVisa();
                    workVisa.fillForm(commandInterface);
                    data = workVisa.getFormData();
                    break;
                case "touristic":
                    TouristicVisa touristicVisa = new TouristicVisa();
                    touristicVisa.fillForm(commandInterface);
                    data = touristicVisa.getFormData();
                    break;
                default:
                    commandInterface.write("Unknown type.");
            }
            if (!data.isEmpty()) {
                formDataSender.send(data);
                data = "";
            }
        }

    }
}
