package ru.ilinovsg.tm.jse49.command;

import org.springframework.stereotype.Component;
import ru.ilinovsg.tm.jse49.command.VoiceCommandInterface;

import java.util.Scanner;

@Component
public class ConsoleCommandInterfaceImpl implements VoiceCommandInterface {
    Scanner scanner = new Scanner(System.in);

    @Override
    public String writeAndRead(String text) {
        System.out.println(text);
        return scanner.nextLine();
    }

    @Override
    public void write(String text) {
        System.out.println(text);
    }
}
