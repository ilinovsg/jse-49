package ru.ilinovsg.tm.jse49.command;

public interface CommandInterface {
    String writeAndRead(String text);

    void write(String text);

}
