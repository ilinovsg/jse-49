package ru.ilinovsg.tm.jse49.command;

import org.springframework.stereotype.Component;
import ru.ilinovsg.tm.jse49.command.VoiceCommandInterface;

@Component
public class VoiceCommandInterfaceImpl implements VoiceCommandInterface {
    @Override
    public String writeAndRead(String text) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void write(String text) {
        throw new UnsupportedOperationException();
    }
}
